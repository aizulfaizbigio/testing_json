package id.bigio.kementan.attendance_machine.services.impl;

import com.querydsl.core.QueryResults;
import com.querydsl.jpa.impl.JPAQuery;
import id.bigio.kementan.attendance_machine.models.custom_response.ReportAllowance;
import id.bigio.kementan.attendance_machine.models.custom_response.VReportAttendance;
import id.bigio.kementan.attendance_machine.models.custom_response.VReportAttendanceDetail;
import id.bigio.kementan.attendance_machine.models.*;
import id.bigio.kementan.attendance_machine.queryfilters.ReportAllowanceQueryFilter;
import id.bigio.kementan.attendance_machine.queryfilters.ReportAttendanceDetailQueryFilter;
import id.bigio.kementan.attendance_machine.queryfilters.ReportAttendanceQueryFilter;
import id.bigio.kementan.attendance_machine.queryfilters.ReportEmployeeTransactionQueryFilter;
import id.bigio.kementan.attendance_machine.repositories.MachineLogRepository;
import id.bigio.kementan.attendance_machine.services.LogEmployeeAbsentService;
import id.bigio.kementan.attendance_machine.services.MachineLogService;
import id.bigio.kementan.attendance_machine.services.ReportService;
import id.bigio.kementan.attendance_machine.utils.Constant;
import id.bigio.kementan.attendance_machine.utils.CustomPageImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    private static final Logger LOG = LogManager.getLogger(ReportServiceImpl.class);

    @Autowired MachineLogRepository machineLogRepository;

    @Autowired LogEmployeeAbsentService logEmployeeAbsentService;

    @Autowired MachineLogService machineLogService;

    @PersistenceContext EntityManager entityManager;

    @Override
    public Page<VReportEmployeeTransaction> getReportEmployeeTransaction(ReportEmployeeTransactionQueryFilter qf) {

        QVReportEmployeeTransaction qvReportEmployeeTransaction = QVReportEmployeeTransaction.vReportEmployeeTransaction;

        JPAQuery<VReportEmployeeTransaction> query = new JPAQuery<VReportEmployeeTransaction>(entityManager).from(qvReportEmployeeTransaction)
                .limit(qf.getLength())
                .offset(qf.getStart());
        if(StringUtils.hasLength(qf.getPositionId())){
            query.where(qvReportEmployeeTransaction.positionId.eq(qf.getPositionId()));
        }

        if(StringUtils.hasLength(qf.getDepartmentId())){
            query.where(qvReportEmployeeTransaction.departmentId.eq(qf.getDepartmentId()));
        }

        if(StringUtils.hasLength(qf.getEmployeeName())){
            query.where(qvReportEmployeeTransaction.employeeName.containsIgnoreCase(qf.getEmployeeName()));
        }

        if(StringUtils.hasLength(qf.getEmployeeNip())){
            query.where(qvReportEmployeeTransaction.employeeNip.containsIgnoreCase(qf.getEmployeeNip()));
        }

        if(qf.getStartDate() != null && qf.getEndDate() != null){
            query.where(qvReportEmployeeTransaction.date.between(qf.getStartDate(), qf.getEndDate()));
            query.orderBy(qvReportEmployeeTransaction.date.asc());
        } else if(qf.getStartDate()!=null){
            query.where(qvReportEmployeeTransaction.date.after(qf.getStartDate()).or(qvReportEmployeeTransaction.date.eq(qf.getStartDate())));
            query.orderBy(qvReportEmployeeTransaction.date.asc());
        } else if(qf.getEndDate()!=null){
            query.where(qvReportEmployeeTransaction.date.before(qf.getEndDate()).or(qvReportEmployeeTransaction.date.eq(qf.getEndDate())));
            query.orderBy(qvReportEmployeeTransaction.date.asc());
        }else{
            query.orderBy(qvReportEmployeeTransaction.date.desc());
        }

        if(StringUtils.hasLength(qf.getSearchText())){
            query.where(qvReportEmployeeTransaction.employeeName.containsIgnoreCase(qf.getSearchText()).or(qvReportEmployeeTransaction.employeeNip.like(qf.getSearchText() + "%")));
        }

        QueryResults<VReportEmployeeTransaction> results = query.fetchResults();

        return new CustomPageImpl<VReportEmployeeTransaction>(results.getResults(), qf.pageable(), results.getTotal());
    }

    @Override
    public Page<VReportAttendance> getReportAttendance(ReportAttendanceQueryFilter qf) {

        QEmployee qEmployee = QEmployee.employee;

        JPAQuery<Employee> query = new JPAQuery<Employee>(entityManager).from(qEmployee)
                .limit(qf.getLength())
                .offset(qf.getStart());

        query.where(qEmployee.deleted.isFalse());

        if(StringUtils.hasLength(qf.getPositionId())){
            query.where(qEmployee.positionId.eq(qf.getPositionId()));
        }

        if(StringUtils.hasLength(qf.getDepartmentId())){
            query.where(qEmployee.departmentId.eq(qf.getDepartmentId()));
        }

        if(StringUtils.hasLength(qf.getEmployeeName())){
            query.where(qEmployee.employeeName.containsIgnoreCase(qf.getEmployeeName()));
        }

        if(StringUtils.hasLength(qf.getEmployeeNip())){
            query.where(qEmployee.employeeNip.containsIgnoreCase(qf.getEmployeeNip()));
        }

        if(StringUtils.hasLength(qf.getSearchText())){
            query.where(qEmployee.employeeName.containsIgnoreCase(qf.getSearchText()).or(qEmployee.employeeNip.like(qf.getSearchText() + "%")));
        }

        QueryResults<Employee> resultEmployee = query.fetchResults();

        List<Employee> employees = resultEmployee.getResults();
        List<VReportAttendance> vReportAttendances = new ArrayList<>();

        for(Employee employee : employees){
            VReportAttendance vReportAttendance = new VReportAttendance();
            vReportAttendance.setEmployeeId(employee.getId());
            vReportAttendance.setEmployeeNip(employee.getEmployeeNip());
            vReportAttendance.setEmployeeName(employee.getEmployeeName());
            vReportAttendance.setDepartmentId(employee.getDepartmentId());
            vReportAttendance.setPositionId(employee.getPositionId());
            vReportAttendance.setPositionName(employee.getPositionName());
            vReportAttendance.setDepartmentName(employee.getDepartmentName());

            ReportAttendanceQueryFilter queryFilter = new ReportAttendanceQueryFilter();
            queryFilter.setEmployeeId(employee.getId());

            if(qf.getStartDate() != null && qf.getEndDate() != null){
                queryFilter.setStartDate(qf.getStartDate());
                queryFilter.setEndDate(qf.getEndDate());
            }

            vReportAttendance.setAbsentTotal(logEmployeeAbsentService.countAbsent(queryFilter));
            vReportAttendance.setEducationPermitTotal(logEmployeeAbsentService.countEducationPermit(queryFilter));
            vReportAttendance.setSickPermitTotal(logEmployeeAbsentService.countSickPermit(queryFilter));
            vReportAttendance.setHolidayTotal(logEmployeeAbsentService.countHoliday(queryFilter));
            vReportAttendance.setOutsidePermitTotal(logEmployeeAbsentService.countOutsidePermit(queryFilter));
            vReportAttendance.setOtherPermitTotal(logEmployeeAbsentService.countOther(queryFilter));
            vReportAttendance.setLateTotal(logEmployeeAbsentService.countLate(queryFilter));
            vReportAttendance.setEarlyTotal(logEmployeeAbsentService.countEarly(queryFilter));
            vReportAttendance.setPresentTotal(machineLogService.getOnTimeTotal(queryFilter.getEmployeeId(),
                    queryFilter.getStartDate(),queryFilter.getEndDate()));
            vReportAttendances.add(vReportAttendance);
        }

        return new CustomPageImpl<VReportAttendance>(vReportAttendances, qf.pageable(), resultEmployee.getTotal());
    }

    @Override
    public Page<VReportAttendanceDetail> getReportAttendanceDetail(ReportAttendanceDetailQueryFilter qf) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        Integer month = calendar.get(Calendar.MONTH) + 1;
        Integer year = calendar.get(Calendar.YEAR);

        QEmployee qEmployee = QEmployee.employee;

        JPAQuery<Employee> query = new JPAQuery<Employee>(entityManager).from(qEmployee)
                .limit(qf.getLength())
                .offset(qf.getStart());

        query.where(qEmployee.deleted.isFalse());

        query.where(qEmployee.active.isTrue());

        if(StringUtils.hasLength(qf.getSearchText())){
            query.where(qEmployee.employeeName.containsIgnoreCase(qf.getSearchText()));
        }

        if(StringUtils.hasLength(qf.getDepartmentId())){
            query.where(qEmployee.departmentId.eq(qf.getDepartmentId()));
        }

        if(StringUtils.hasLength(qf.getPositionId())){
            query.where(qEmployee.positionId.eq(qf.getPositionId()));
        }

        if(qf.getMonth() != null && qf.getYear() != null){
            month = qf.getMonth();
            year = qf.getYear();
        }

        QueryResults<Employee> employeeResult = query.fetchResults();

        List<Employee> employeeList = employeeResult.getResults();
        List<VReportAttendanceDetail> reportAttendanceDetails = new ArrayList<>();

        String customWhere = "";

        if(qf.getAbsentType() != null){
            customWhere = " AND status = '"+ Constant.convertListOf(Constant.LIST_FILTER_ATTENDANCE_DETAIL, Constant.LIST_FILTER_ATTENDANCE_DETAIL_LABEL, qf.getAbsentType()) +"'";
        }

        for(Employee employee : employeeList){
            VReportAttendanceDetail vReportAttendanceDetail = new VReportAttendanceDetail();
            vReportAttendanceDetail.setEmployeeId(employee.getId());
            vReportAttendanceDetail.setEmployeeName(employee.getEmployeeName());
            vReportAttendanceDetail.setEmployeeNip(employee.getEmployeeNip());
            vReportAttendanceDetail.setListDateAbsent(logEmployeeAbsentService.getAttendanceDetail(employee.getId(), month, year, customWhere));

            reportAttendanceDetails.add(vReportAttendanceDetail);
        }

        return new CustomPageImpl<VReportAttendanceDetail>(reportAttendanceDetails, qf.pageable(), employeeResult.getTotal());
    }

    @Override
    public Page<ReportAllowance> getReportAllowance(ReportAllowanceQueryFilter qf) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        Integer month = calendar.get(Calendar.MONTH) + 1;
        Integer year = calendar.get(Calendar.YEAR);

        if(qf.getMonth() != null && qf.getYear() != null){
            month = qf.getMonth();
            year = qf.getYear();
        }

        String customWhere = "";
        if(StringUtils.hasLength(qf.getPositionId())){
            customWhere += " AND y.position_id = '" + qf.getPositionId() + "'";
        }

        if(StringUtils.hasLength(qf.getGolonganId())){
            customWhere += " AND y.golongan_id = '" + qf.getGolonganId() + "'";
        }

        if(StringUtils.hasLength(qf.getSearchText())){
            customWhere += " AND (y.employee_nip ilike '"+ qf.getSearchText() +"%' OR y.employee_name ilike '%"+ qf.getSearchText() +"%')";
        }

        if(StringUtils.hasLength(qf.getEmployeeId())){
            customWhere += " AND (y.employee_id = '"+ qf.getEmployeeId() +"')";
        }

        if(StringUtils.hasLength(qf.getDepartmentId())){
            customWhere+= " AND (y.department_id = '" + qf.getDepartmentId() + "')";
        }

        String customLimit = " LIMIT " + qf.getLength() + " OFFSET " + qf.getStart();

        List<ReportAllowance> reportAllowanceList = logEmployeeAbsentService.getAllowanceReport(month, year, customWhere, customLimit);
        Integer countAllowance = logEmployeeAbsentService.countAllowanceReport(month, year, customWhere);

        return new CustomPageImpl<ReportAllowance>(reportAllowanceList, qf.pageable(), countAllowance);
    }

    @Override
    public ByteArrayInputStream getReportEmployeeTransactionByteArray(List<VReportEmployeeTransaction> reportEmployeeTransactionList) throws IOException {
        String[] Columns = {"No", "Tanggal", "NIP" ,"Nama Pegawai",  "Jabatan", "Departemen",
                "Waktu Checkin", "Waktu Checkout"};
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper creationHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet();
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            //Header Row
            Row headerRow = sheet.createRow(0);
            for (int col = 0; col<Columns.length; col++){
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(Columns[col]);
                cell.setCellStyle(headerCellStyle);
            }

            CellStyle dateCellStyle = workbook.createCellStyle();
            dateCellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("dd/MM/yyyy"));

            CellStyle noDataCellStyle = workbook.createCellStyle();
            noDataCellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
            noDataCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            CellStyle notOnTimeDataCellStyle = workbook.createCellStyle();
            notOnTimeDataCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            notOnTimeDataCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            CellStyle onTimeDataCellStyle = workbook.createCellStyle();
            onTimeDataCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
            onTimeDataCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            int rowIndex = 1;
            for(VReportEmployeeTransaction vReportEmployeeTransaction : reportEmployeeTransactionList){
                Row row = sheet.createRow(rowIndex);

                row.createCell(0).setCellValue(rowIndex);
                Cell dateCell = row.createCell(1);
                dateCell.setCellValue(vReportEmployeeTransaction.getDate());
                dateCell.setCellStyle(dateCellStyle);
                row.createCell(2).setCellValue(vReportEmployeeTransaction.getEmployeeNip());
                row.createCell(3).setCellValue(vReportEmployeeTransaction.getEmployeeName());
                row.createCell(4).setCellValue(vReportEmployeeTransaction.getPositionName());
                row.createCell(5).setCellValue(vReportEmployeeTransaction.getDepartmentName());
                Cell checkinCell = row.createCell(6);
                if(StringUtils.hasLength(vReportEmployeeTransaction.getCheckIn())){
                    checkinCell.setCellValue(vReportEmployeeTransaction.getCheckIn());
                    if(vReportEmployeeTransaction.getLate()==0 || vReportEmployeeTransaction.getLate()==null){
                        checkinCell.setCellStyle(onTimeDataCellStyle);
                    }else{
                        checkinCell.setCellStyle(notOnTimeDataCellStyle);
                    }
                } else{
                    checkinCell.setCellValue("Belum Check in");
                    checkinCell.setCellStyle(noDataCellStyle);
                }
                Cell checkoutCell = row.createCell(7);
                if(StringUtils.hasLength(vReportEmployeeTransaction.getCheckOut())){
                    checkoutCell.setCellValue(vReportEmployeeTransaction.getCheckOut());
                    if(vReportEmployeeTransaction.getEarly()==0 || vReportEmployeeTransaction.getEarly()==null){
                        checkoutCell.setCellStyle(onTimeDataCellStyle);
                    }else{
                        checkoutCell.setCellStyle(notOnTimeDataCellStyle);
                    }
                }else {
                    checkoutCell.setCellValue("Belum Check out");
                    checkoutCell.setCellStyle(noDataCellStyle);
                }
                rowIndex++;
            }
            for(int col = 0; col<Columns.length;col++){
                if(col==4 || col==5) continue;
                sheet.autoSizeColumn(col);
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    @Override
    public Page<VReportEmployeeTransaction> getReportEmployeeTransactionAll(ReportEmployeeTransactionQueryFilter qf) {
        QVReportEmployeeTransaction qvReportEmployeeTransaction = QVReportEmployeeTransaction.vReportEmployeeTransaction;

        JPAQuery<VReportEmployeeTransaction> query = new JPAQuery<VReportEmployeeTransaction>(entityManager).from(qvReportEmployeeTransaction)
                .offset(qf.getStart());

        QueryResults<VReportEmployeeTransaction> results = query.fetchResults();

        return new CustomPageImpl<VReportEmployeeTransaction>(results.getResults(), qf.pageable(), results.getTotal());
    }

    @Override
    public ByteArrayInputStream generateAttendanceToExcel(ReportAttendanceQueryFilter qf) throws IOException {
        Page<VReportAttendance> page = this.getReportAttendance(qf);
        String[] Columns = {"No", "Nama Pegawai", "NIP" ,"Jabatan", "Departemen", "Data Ketidakhadiran"
                , "Total Kehadiran"};
        String[] columns1 = {"Absen", "Terlambat", "Izin"};
        String[] columns2 = {"Pendidikan", "Dinas Luar Kota", "Urusan Lainnya", "Sakit"};

        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            Sheet sheet = workbook.createSheet();
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
            headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            Row headerRow = sheet.createRow(0);
            for (int col = 0; col<12; col++){
                if(col>5){
                    Cell cell = headerRow.createCell(col);
                    cell.setCellValue(Columns[col-5]);
                    cell.setCellStyle(headerCellStyle);
                    continue;
                }
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(Columns[col]);
                cell.setCellStyle(headerCellStyle);
                if(col==5) col = 10;
            }

            Row headerSecondRow = sheet.createRow(1);
            for (int col = 5; col<8; col++){
                Cell cell = headerSecondRow.createCell(col);
                cell.setCellValue(columns1[col-5]);
                cell.setCellStyle(headerCellStyle);
            }

            Row headerThirdRow = sheet.createRow(2);
            for (int col = 7; col<11; col++){
                Cell cell = headerThirdRow.createCell(col);
                cell.setCellValue(columns2[col-7]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowIndex = 3;
            for(VReportAttendance reportAttendance: page.getContent()){
                Row row = sheet.createRow(rowIndex);

                row.createCell(0).setCellValue(rowIndex-2);
                row.createCell(1).setCellValue(reportAttendance.getEmployeeName());
                row.createCell(2).setCellValue(reportAttendance.getEmployeeNip());
                row.createCell(3).setCellValue(reportAttendance.getPositionName());
                row.createCell(4).setCellValue(reportAttendance.getDepartmentName());
                row.createCell(5).setCellValue(reportAttendance.getAbsentTotal());
                row.createCell(6).setCellValue(reportAttendance.getLateTotal());
                row.createCell(7).setCellValue(reportAttendance.getEducationPermitTotal());
                row.createCell(8).setCellValue(reportAttendance.getOutsidePermitTotal());
                row.createCell(9).setCellValue(reportAttendance.getOtherPermitTotal());
                row.createCell(10).setCellValue(reportAttendance.getSickPermitTotal());
                row.createCell(11).setCellValue(reportAttendance.getPresentTotal());

                rowIndex++;
            }

            for (int x = 0; x<=4; x++){
                sheet.addMergedRegion(new CellRangeAddress(0,2,x,x));
            }
            //Data Ketidakhadiran
            sheet.addMergedRegion(new CellRangeAddress(0,0,5,10));

            //Absen
            sheet.addMergedRegion(new CellRangeAddress(1,2,5,5));
            //Absen
            sheet.addMergedRegion(new CellRangeAddress(1,2,6,6));
            //Izin
            sheet.addMergedRegion(new CellRangeAddress(1,1,7,10));
            //Total Kehadiran
            sheet.addMergedRegion(new CellRangeAddress(0,2,11,11));

            for(int col = 0; col<12;col++){
                if(col==3) continue;
                sheet.autoSizeColumn(col,true);
            }
            int width = ((int)(30 * 1.14388)) * 256;
            sheet.setColumnWidth(3,width);
            workbook.write(out);

            return new ByteArrayInputStream(out.toByteArray());
        }

    }

    @Override
    public ByteArrayInputStream generateAllowanceToExcel(ReportAllowanceQueryFilter qf) throws IOException {

        Page<ReportAllowance> page = this.getReportAllowance(qf);
        String[] Columns = {"No", "Nama Pegawai", "NIP" ,"Jabatan", "Golongan", "Akumulasi Data Ketidakhadiran (Hari)"
                , "Denda (%)", "Total Tunjangan"};
        String[] columnsUnmerge = {"Absen", "Terlambat", "Pulang Cepat", "Pendidikan"};

        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            Sheet sheet = workbook.createSheet();
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
            headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            Row headerRow = sheet.createRow(0);
            for (int col = 0; col<11; col++){
                if(col>5){
                    Cell cell = headerRow.createCell(col);
                    cell.setCellValue(Columns[col-3]);
                    cell.setCellStyle(headerCellStyle);
                    continue;
                }
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(Columns[col]);
                cell.setCellStyle(headerCellStyle);
                if(col==5)col=8;
            }



            Row headerUnmerged = sheet.createRow(1);
            for(int col = 5; col<(columnsUnmerge.length+5);col++){
                Cell cell = headerUnmerged.createCell(col);
                cell.setCellValue(columnsUnmerge[col-5]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowIndex = 2;
            for(ReportAllowance reportAllowance: page.getContent()){
                Row row = sheet.createRow(rowIndex);

                row.createCell(0).setCellValue(rowIndex-1);
                row.createCell(1).setCellValue(reportAllowance.getEmployeeName());
                row.createCell(2).setCellValue(reportAllowance.getEmployeeNip());
                row.createCell(3).setCellValue(reportAllowance.getPositionName());
                row.createCell(4).setCellValue(reportAllowance.getGolonganName());
                row.createCell(5).setCellValue(reportAllowance.getAbsentTotal());
                row.createCell(6).setCellValue(reportAllowance.getLateTotal());
                row.createCell(7).setCellValue(reportAllowance.getEarlyTotal());
                row.createCell(8).setCellValue(reportAllowance.getEducationTotal());
                row.createCell(9).setCellValue(reportAllowance.getTotalAllowancePercentage());
                row.createCell(10).setCellValue(reportAllowance.getTotalAllowance());

                rowIndex++;
            }


            for (int x = 0; x<=4; x++){
                sheet.addMergedRegion(new CellRangeAddress(0,1,x,x));
            }
            sheet.addMergedRegion(new CellRangeAddress(0,0,5,8));
            sheet.addMergedRegion(new CellRangeAddress(0,1,9,9));
            sheet.addMergedRegion(new CellRangeAddress(0,1,10,10));

            for(int col = 0; col<11;col++){
                sheet.autoSizeColumn(col,true);
            }

            workbook.write(out);

            return new ByteArrayInputStream(out.toByteArray());
        }
    }
}
