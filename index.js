var datatableUser;

$(document).ready(function () {
    dataTable();

    $("#search").on("keyup paste", function () {
        tableMasjid.search($(this).val()).draw();
    });
})

function dataTable() {

    console.log("hahahaha")

    $("#dataTable")
        .DataTable()
        .clear()
        .destroy();
    datatableUser = $("#dataTable").DataTable({
        processing: true,
        serverSide: true,
        bLengthChange: false,
        searching: false,
        orderable: [
            [0, "desc"],
            [1, "asc"]
        ],
        language: {
            emptyTable: "Data tidak tersedia",
            zeroRecords: "Tidak ada data yang ditemukan",
            infoFiltered: "",
            infoEmpty: "",
            paginate: {
                previous: "‹",
                next: "›"
            },
            info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ Pengguna Masjid",
            aria: {
                paginate: {
                    previous: "Previous",
                    next: "Next"
                }
            }
        },
        ajax: {
            url: "/api/user/datatable",
            contentType: "application/json",
            type: "POST",
            data: function (d) {
                // console.log(d);
                var dataparam = {
                    draw: d.draw,
                    page: d.start / d.length + 1,
                    length: d.length,
                    role: "mobile",
                    search_text: $("#search").val()
                };
                return JSON.stringify(dataparam);
            },
            dataSrc: function (response) {
                console.log(response.data)
                return response.data;
            }
        },
        columns: [{
                data: null,
                "width": "5%"
            },
            {
                data: "name",
                "width": "15%"
            },
            {
                data: "phone_number"
            },
            {
                data: "email"
            },
            {
                data: null,
                "width": "20%"
            }
        ],
        columnDefs: [{
                //id
                targets: 0,
                searchable: false,
                orderable: false,
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).addClass("text-center");
                    $(td).html(datatableUser.page.info().start + row + 1);
                }
            },
            {
                targets: 1,
                searchable: false,
                orderable: false,
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).addClass("text-center");
                }
            },
            {
                targets: 2,
                searchable: false,
                orderable: false,
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).addClass("text-center");
                }
            },
            {
                targets: 3,
                searchable: false,
                orderable: false,
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).addClass("text-center");
                }
            },
            {
                targets: 4,
                searchable: false,
                orderable: false,
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).addClass("text-center");
                    var html =
                        "<button type='button' onClick='detailMasjid(" +
                        rowData.id +
                        ")' class='btn btn-primary btn-sm' style='margin-left: 10px;' id='btn-detail-masjid'><i class='fas fa-eye'></i></button>";
                    $(td).html(html);
                }
            }
        ]
    });
}